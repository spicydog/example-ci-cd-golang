package main

import (
	"fmt"
	"gitlab.com/spicydog/example-ci-cd-golang/stringutil"
)

func main() {
	fmt.Println(stringutil.Reverse("!dlroW olleH"))
}