# Example for GitLab CI/CD with Golang [Go] 

[![build status](https://gitlab.com/spicydog/example-ci-cd-golang/badges/master/build.svg)](https://gitlab.com/spicydog/example-ci-cd-golang/commits/master)
[![coverage report](https://gitlab.com/spicydog/example-ci-cd-golang/badges/master/coverage.svg)](https://gitlab.com/spicydog/example-ci-cd-golang/commits/master)

This project demonstrates how to use GitLab for CI/CD tasks with Golang as an example for my blog on [GitLab CI/CD](https://www.spicydog.org/blog/gitlab-ci-cd-1-introduction/).